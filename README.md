# Stripe Demo for Tiller
Application using [Blockbase](https://github.com/blacksmithstudio/blockbase) and [React](https://github.com/facebook/react)

### Version
1.0.0

### Setup

**Pre-requirements**

To run this app, you'll have to install the following pre-requirements.
- Node.js > 8.7 & NPM (download [here](https://nodejs.org/en/))
- PostgreSQL > 9.5 (download [here](https://www.postgresql.org/))


**Update the config**

Update the `config/default.yml` file to update the DB config according to your local environment.
```yml
dbms : postgresql
postgresql :
    host : localhost
    user : your-username
    password : your-password
    port : 5432
    database : stripedemo
```

**Setup the project**

Run the following commands to install the dependencies and the demo-database.
```sh
$ npm i
$ npm run setup
```

**Run the project**
```sh
$ npm run app
```

Then your app is available on your localhost : http://localhost:1337

Note, default email|password credentials to the dashboard are `admin@tillersystems.com|admin1234`


#### Run the tests (mocha should be install, see Dev Dependencies)
```sh
$ npm run test
```

License
----

(Copyright) 2018 - Alexandre Pereira.


**Free Software, Hell Yeah!**

[Node.js]:https://nodejs.org/en
[NPM]:https://www.npmjs.com