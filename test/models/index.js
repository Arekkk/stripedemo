/**
 * Loading all tests from app.models.*
 */
module.exports = (app) => {
    require('./app.models.administrator')(app)
    require('./app.models.customer')(app)
    require('./app.models.payment')(app)
}
