const assert = require('assert')

/**
 * Executing all tests for app.models.payment.*
 */
module.exports = (app) => {
    describe('app.models.payment Architecture', () => {
        describe(`app.models.payment tests`, () => {
            it('app.models.payment exists', () => {
                assert.equal('function', typeof app.models.payment)
            })

            const Payment = app.models.payment
            let payment = new Payment({ 
                type : 'stripe', 
                customer : 'xxx-xxx', 
                amount : 999, 
                charge : 'xxx-xxxx', 
                token : 'xxx-xxx' 
            })

            it('app.models.payment creation', () => {
                assert.equal('stripe', payment.body().type)
            })

            it('app.models.payment validation ok', () => {
                assert.equal(true, payment.valid())
            })

            it('app.models.payment validation error', () => {
                let pay = new Payment({ type : 98, amount : true })
                assert.equal(false, pay.valid())
            })

            it('app.models.payment save in db', async () => {
                payment = await payment.save()
                assert.equal(true, !!payment.data.id.length && payment.valid())
                await payment.delete()
            })
        })
    })
}
