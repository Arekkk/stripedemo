const assert = require('assert')

/**
 * Executing all tests for app.models.administrator.*
 */
module.exports = (app) => {
    describe('app.models.administrator Architecture', () => {
        describe(`app.models.administrator tests`, () => {
            it('app.models.administrator exists', () => {
                assert.equal('function', typeof app.models.administrator)
            })

            const Administrator = app.models.administrator
            let administrator = new Administrator({ email : 'xyz@tiller.com', password : 'h4sh' })

            it('app.models.administrator creation', () => {
                assert.equal('xyz@tiller.com', administrator.body().email)
            })

            it('app.models.administrator validation ok', () => {
                assert.equal(true, administrator.valid())
            })

            it('app.models.administrator validation error', () => {
                let cst = new Administrator({ email : 11, firstname : true })
                assert.equal(false, cst.valid())
            })

            it('app.models.administrator save in db', async () => {
                administrator = await administrator.save()
                assert.equal(true, !!administrator.data.id.length && administrator.valid())
                await administrator.delete()
            })
        })
    })
}
