const assert = require('assert')

/**
 * Executing all tests for app.models.customer.*
 */
module.exports = (app) => {
    describe('app.models.customer Architecture', () => {
        describe(`app.models.customer tests`, () => {
            it('app.models.customer exists', () => {
                assert.equal('function', typeof app.models.customer)
            })

            const Customer = app.models.customer
            let customer = new Customer({ email : 'xyz@tiller.com' })

            it('app.models.customer creation', () => {
                assert.equal('xyz@tiller.com', customer.body().email)
            })

            it('app.models.customer validation ok', () => {
                assert.equal(true, customer.valid())
            })

            it('app.models.customer validation error', () => {
                let cst = new Customer({ email : 11, firstname : true })
                assert.equal(false, cst.valid())
            })

            it('app.models.customer save in db', async () => {
                customer = await customer.save()
                assert.equal(true, !!customer.data.id.length && customer.valid())
                await customer.delete()
            })
        })
    })
}
