/**
 * Loading all tests from app.controllers.*
 */
module.exports = (app) => {
    require('./app.controllers.payments')(app)
    require('./app.controllers.router')(app)
}
