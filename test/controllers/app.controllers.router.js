const assert = require('assert')

/**
 * Executing all tests for app.controllers.router.*
 */
module.exports = (app) => {
    describe('app.controllers.router.* Architecture', () => {
        describe(`app.controllers.router tests`, () => {
            it('app.controllers.router exists', () => {
                assert.equal('object', typeof app.controllers.router)
            })

            it('app.controllers.router.home exists', () => {
                assert.equal('function', typeof app.controllers.router.home)
            })
        })
    })
}
