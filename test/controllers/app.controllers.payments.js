const assert = require('assert')

/**
 * Executing all tests for app.controllers.payments.*
 */
module.exports = (app) => {
    describe('app.controllers.payments.* Architecture', () => {
        describe(`app.controllers.payments tests`, () => {
            it('app.controllers.payments exists', () => {
                assert.equal('object', typeof app.controllers.payments)
            })

            it('app.controllers.payments.create exists', () => {
                assert.equal('function', typeof app.controllers.payments.create)
            })
            it('app.controllers.payments.list exists', () => {
                assert.equal('function', typeof app.controllers.payments.list)
            })
        })
    })
}
