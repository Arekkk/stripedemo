/**
 * Loading all tests from app.drivers.*
 */
module.exports = function (app) {
    describe('app.drivers tests', function () {
        require('./app.drivers.stripe')(app)
    })
}