const assert = require('assert')


module.exports = app => {
    describe('app.drivers.knex.* Architecture', () => {
        describe(`app.drivers.knex tests`, () => {
            it('app.drivers.knex exists', () => {
                assert.equal('object', typeof app.drivers.knex)
            })
        })
    })
}