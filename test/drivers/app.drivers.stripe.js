const assert = require('assert')


module.exports = app => {
    describe('app.drivers.stripe.* Architecture', () => {
        describe(`app.drivers.stripe tests`, () => {
            it('app.drivers.stripe exists', () => {
                assert.equal('object', typeof app.drivers.stripe)
            })
        })
    })
}