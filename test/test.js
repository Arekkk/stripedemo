const fs = require('fs')
const assert = require('assert')
const blockbase = require('@blacksmithstudio/blockbase')

/**
 * Stripe Demo Unit test charger
 */
describe('Initialization of tests', () => {
   it('app presence', () => {
      assert.equal(true, fs.existsSync(`${__dirname}/../src/app.js`))
   })
})

blockbase({ root : `${__dirname}/../src/` }, (app) => {
    require('./app')(app)
    require('./controllers')(app)
    require('./models')(app)
    require('./drivers')(app)
})
