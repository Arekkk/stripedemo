const assert = require('assert')

/**
 * Loading all tests from app.*
 */
module.exports = (app) => {
    describe('app. Architecture', () => {
        describe(`app.* tests`, () => {
            it('app.* exists', () => {
                assert.equal('object', typeof app)
            })

            it('app.config.* exists', () => {
                assert.equal('object', typeof app.config)
            })

            it('app.config.name value', () => {
                assert.equal('string', typeof app.config.name)
            })

            it('app.drivers.* exists', () => {
                assert.equal('object', typeof app.drivers)
            })

            it('app.models.* exists', () => {
                assert.equal('object', typeof app.models)
            })

            it('app.controllers.* exists', () => {
                assert.equal('object', typeof app.controllers)
            })
        })
    })
}
