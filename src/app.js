/**
 * Blockbase Strip Demo Init File
 * @author Alexandre Pereira <alex@gooood.biz>
 */

const blockbase = require('@blacksmithstudio/blockbase')

blockbase({ root : __dirname }, async (app) => {
    app.drivers.express.route()
    app.drivers.express.listen()
    // ...
})