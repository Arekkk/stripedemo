/**
 * Payment model
 * @namespace app.models.payment
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} model
 */
module.exports = (app) => {
    const Model = app.models._model
    const Knex = app.drivers.knex

    return class Payment extends Model {
        /**
         * main constructor
         * @param {Object} data - data to param the model
         */
        constructor(data) {
            super({ type: 'payment' })
            this.orm = Knex('payments')

            if (data)
                this.data = data
        }

        /**
         * list the payments
         */
        async list(){
            try{
                return await Knex.select('*')
                                .from(`${this.params.type}s`)
                                .leftJoin('customers', 'payments.customer', 'customers.id')
                                .orderBy('payments.created_at', 'desc')
            } catch(error){
                throw error
            }
        }
    }
}
