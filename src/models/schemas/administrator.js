const Joi = require('joi')
const moment = require('moment')
const shortid = require('shortid')

/**
 * Customer validation schema (with Joi)
 * @author Alexandre Pereira <alex@gooood.biz>
 * @returns {Object} Joi schema
 */
module.exports = Joi.object().keys({
    id         : Joi.string().max(128).default(() => shortid.generate(), 'customer id generated'),
    email      : Joi.string().max(512),
    password   : Joi.string().max(1024),
    logged_at  : Joi.date().default(() => moment().format(), 'date logged'),
    created_at : Joi.date().default(() => moment().format(), 'date created'),
    updated_at : Joi.date().default(() => moment().format(), 'date updated'),
})
