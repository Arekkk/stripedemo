const Joi = require('joi')
const moment = require('moment')
const shortid = require('shortid')

/**
 * Payment validation schema (with Joi)
 * @author Alexandre Pereira <alex@gooood.biz>
 * @returns {Object} Joi schema
 */
module.exports = Joi.object().keys({
    id         : Joi.string().max(128).default(() => shortid.generate(), 'payment id generated'),
    type       : Joi.string().max(128).optional(),
    token      : Joi.string().max(512).optional(),
    charge     : Joi.string().max(512).optional(),
    customer   : Joi.string().max(128),
    amount     : Joi.number().optional(),
    created_at : Joi.date().default(() => moment().format(), 'date created'),
    updated_at : Joi.date().default(() => moment().format(), 'date updated'),
})
