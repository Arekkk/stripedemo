/**
 * Customer model
 * @namespace app.models.customer
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} model
 */
module.exports = (app) => {
    const Model = app.models._model
    const Knex = app.drivers.knex

    return class Customer extends Model {
        /**
         * main constructor
         * @param {Object} data - data to param the model
         */
        constructor(data) {
            super({ type: 'customer' })
            this.orm = Knex('customers')

            if (data)
                this.data = data
        }
    }
}
