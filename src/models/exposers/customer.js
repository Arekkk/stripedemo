/**
 * Customer Exposer system for models
 * @author Alexandre Pereira <alex@gooood.biz>
 *
 * @returns {Object} exposer
 */
module.exports = {
    'public': [
        'id'
    ],

    'private': [
        'id',
        'firstname',
        'lastname',
        'email',
        'logged_at',
        'created_at',
        'updated_at'
    ]
}
