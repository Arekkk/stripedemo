/**
 * Payment Exposer system for models
 * @author Alexandre Pereira <alex@gooood.biz>
 *
 * @returns {Object} exposer
 */
module.exports = {
    'public': [
        'id'
    ],

    'private': [
        'id',
        'token',
        'amount',
        'customer',
        'created_at',
        'updated_at'
    ]
}
