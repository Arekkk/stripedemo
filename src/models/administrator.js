/**
 * Administrator model
 * @namespace app.models.administrator
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} model
 */
module.exports = (app) => {
    const Model = app.models._model
    const Knex = app.drivers.knex

    return class Administrator extends Model {
        /**
         * main constructor
         * @param {Object} data - data to param the model
         */
        constructor(data) {
            super({ type: 'administrator' })
            this.orm = Knex('administrators')

            if (data)
                this.data = data
        }
    }
}
