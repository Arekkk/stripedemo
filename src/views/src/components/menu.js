import React from 'react'
import ReactDOM from 'react-dom'

class Menu extends React.Component {
	constructor(props){
		super(props)
    }
    
    logout(e){
        localStorage.removeItem('token')
        location.reload()
    }
	
	render(){
		return (<div className="site-wrapper_left-col">
                    <a href="/dash" className="logo"><img src="https://www.tillersystems.com/wp-content/uploads/2016/10/logo_light@2x.png" alt="Tiller"/></a>
                    <div className="left-nav">
                    { this.props.logged ? (
                        <div>
                            <a href="/dash" className="active"><i className="fa fa-home"></i>Payments</a>
                            <a href="javascript:;" onClick={this.logout}><i className="fa fa-cog"></i>Logout</a>
                        </div>
                    ) : (
                        <a href="/dash" ><i className="fa fa-cog"></i>Login</a>
                    ) }
                    </div>
                </div>)
	}
}

export default Menu