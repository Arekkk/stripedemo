import React from 'react'
import ReactDOM from 'react-dom'

import Moment from 'moment'

class Payments extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			error: null,
			loaded: false,
			payments: []
		}
	}
	
	componentDidMount() {
		fetch('/payments')
			.then(res => res.json())
			.then(
				(payments) => {
					this.setState({ loaded : true, payments })
				},
				(error) => {
					this.setState({ loaded : true, error })
				}
			)
	}

	render(){
		const { error, loaded, payments } = this.state;
		if (error) {
			return <div>Error: {error.message}</div>;
		} else if (!loaded) {
			return <div>Loading...</div>;
		} else if (!payments.length) {
			return <div>No payment processed yet.</div>
		} else {
			return (
				<div>
					<h1>Recent Payments</h1>
					<table cellSpacing="0" className="table table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Type</th>
							<th>Customer Email</th>
							<th>Revenue</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{ 
							payments.map(
								payment => (
									<tr key={ payment.id }>
										<th>{ payment.id }</th>
										<td>{ payment.type }</td>
										<td>{ payment.email }</td>
										<td>{ `${payment.amount/100}€` }</td>
										<td>{ Moment(payment.created_at).format('MMMM Do YYYY, h:mm') }</td>
									</tr>
								)
							)
						}
					</tbody>
					</table>
				</div>
			)
		}
	}
}

export default Payments