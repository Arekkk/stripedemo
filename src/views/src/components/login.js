import React from 'react'
import ReactDOM from 'react-dom'

class Login extends React.Component {
	constructor(props){
        super(props)
        this.state = {
            email : '',
            password : ''
        }
    }
    
    onChange(e){
        const obj = {}
        obj[e.target.name] = e.target.value;
        this.setState(obj)
    }

    onSubmit(e){
        e.preventDefault()
        const { email, password } = this.state

        fetch('/auth', {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password })
        }).then((response) => response.json())
        .then((responseJson) => {
            if(!responseJson.success)
                return alert(responseJson.message)

            localStorage.setItem('token', responseJson.token)
            location.reload()
        })
    }
	
	render(){
        const { email, password } = this.state

		return (
            <div>
                <h1>Login</h1>
                <form id="login" onSubmit={this.onSubmit.bind(this)}>
                    <input type="text" key="email" placeholder="Email" name="email" value={email} onChange={this.onChange.bind(this)}/>
                    <input type="password" key="password" placeholder="Password" name="password" value={password} onChange={this.onChange.bind(this)}/>
                    <input type="submit" value="Login"/>
                </form>
            </div>
        )
	}
}

export default Login