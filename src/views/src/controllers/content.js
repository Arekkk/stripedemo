import React from 'react'
import ReactDOM from 'react-dom'

import Login from '../components/login'
import Payments from '../components/payments'

const components = { Login, Payments }

class Content extends React.Component {
    render(){
        const Type = components[this.props.type]
        return <Type />
    }
}

export default Content