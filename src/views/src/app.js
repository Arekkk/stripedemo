import React from 'react'
import ReactDOM from 'react-dom'

import Menu from './components/menu'
import Content from './controllers/content'

class App extends React.Component {
	constructor(props){
		super(props)
		this.state = {
      logged : false,
      type : 'Login'
		}
  }

  componentWillMount(){
    if(!localStorage.getItem('token')){
      this.setState({ type : 'Login' })
    } else {
      this.setState({ type : 'Payments', logged : true })
    }
  }
	
	render(){
		return (
      <div className="wrapper">
        <div className="wrapper_container">
          <div className="site-wrapper active" id="target">
            <Menu logged={this.state.logged} />
            <div className="site-wrapper_top-bar"></div>
            <div className="row">
              <Content type={this.state.type} />
            </div>
          </div>  
        </div>
      </div>
    )
	}
}

ReactDOM.render(<App/>, document.getElementById('dashboard'))

