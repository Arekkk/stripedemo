const CryptoJS = require('crypto-js')
const bcrypt = require('bcrypt')

/**
 * Auth Controller
 * @namespace app.controllers.auth
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} controller
 */
module.exports = (app) => {
    const Config = app.config
    const Logger = app.drivers.logger 

    const Administrator = app.models.administrator

    return {
        /**
         * login an admin
         * @async
         * @param {Object} req - express req object
         * @param {Object} res - express res object
         */
        async login(req, res){
            const { email, password } = req.body

            try{
                let result = await new Administrator().orm.where('email', email)
                if(!result.length)
                    return res.status(422).send({ success : false, message : 'Unknown login' })

                let administrator = new Administrator(result[0])
                if(!bcrypt.compareSync(password, administrator.data.password))
                    return res.status(422).send({ success : false, message : 'Unknown login or password' })
                
                let token = CryptoJS.AES.encrypt(JSON.stringify({
                    id : administrator.data.id,
                    created_at : administrator.data.created_at.toString()
                }), Config.cryptoKey)

                res.status(200).send({ success : true, token : token.toString() })
            } catch(error){
                Logger.error('Administrator Login', error)
                res.status(500).send({ success : false, message : error })
            }
        }
    }
}