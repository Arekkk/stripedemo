/**
 * Payments Controller
 * @namespace app.controllers.payments
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} controller
 */
module.exports = (app) => {
    const Config = app.config

    const Logger = app.drivers.logger 
    const Stripe = app.drivers.stripe

    const Customer = app.models.customer
    const Payment = app.models.payment

    return {
        /**
         * process the payment, receiving the token from stripe
         * @async
         * @param {Object} req - express req object
         * @param {Object} res - express res object
         */
        async create(req, res){
            const { stripeEmail, stripeToken } = req.body

            try{
                let result = await new Customer().orm.where('email', stripeEmail)
                let customer = new Customer( !result.length ? { email : stripeEmail } : result[0] )
                
                // if customer doesn't exist then save it
                if(!result.length)
                    customer = await customer.save()

                // validate Stripe token
                let validation = await Stripe.validate(stripeToken)
                if(!validation.valid) 
                    return res.status(422).send({ 
                        success : false, 
                        message : 'an error occured when validating the token' 
                    })

                // charge the customer with the validated token
                let charge = await Stripe.charge(999, Config.stripe.default_currency, validation.data.id)
                
                // save the payment on our database
                let payment = new Payment({
                    type : 'stripe',
                    token : validation.data.id,
                    charge : charge.id,
                    customer : customer.data.id,
                    amount : charge.amount
                })
                await payment.save()

                res.redirect('/payments/success')
            } catch(error){
                Logger.error('Payment Processing', error)
                res.status(500).send({ success : false, message : 'Something is wrong with Payment' })
            }
        },

        /**
         * list the payments
         * @async
         * @param {Object} req - express req object
         * @param {Object} res - express res object
         */
        async list(req, res){
            try{
                let payments = await new Payment().list()
                res.status(200).send(payments)
            } catch(error){
                Logger.error('Payments List', error)
                res.status(500).send({ success : false, message : 'Something is wrong with Payments List' })
            }
        }
    }
}