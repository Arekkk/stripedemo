/**
 * Router Controller
 * @namespace app.controllers.router
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} controller
 */
module.exports = (app) => {
    const Config = app.config
    const Logger = app.drivers.logger 

    return {
        /**
         * show the homepage with custom infos
         * @async
         * @param {Object} req - express req object
         * @param {Object} res - express res object
         */
        home(req, res){
            res.render(`${app.root}/views/index.twig`, { Config })
        }
    }
}