let stripe = require('stripe')

/**
 * Stripe Driver
 * @namespace app.drivers.stripe
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} driver
 */
module.exports = (app) => {
    const Config = app.config
    stripe = stripe(Config.stripe.secret_key)

    return {
        /**
         * validate a stripe token
         * @async
         * @param {string} token - strip token to validate
         * 
         * @returns {Object} validation { valid : bool, data : object }
         */
        async validate(token){
            return new Promise((resolve, reject) => {
                stripe.tokens.retrieve(token, (err, data) => {
                    if(err) return reject(err)
                    resolve({
                        valid : (data && !!data.id),
                        data
                    })
                })
            })
        },

        /**
         * charge the client token
         * @async
         * @param {number} amount - amount to charge
         * @param {string} currency - appliable currency
         * @param {string} token - strip token to charge
         * 
         * @returns {Object} charge
         */
        async charge(amount, currency, token){
            return stripe.charges.create({
                amount,
                currency,
                description: 'Example charge for Tiller',
                source: token
            })
        }
    }
}
