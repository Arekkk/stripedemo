const knex = require('knex')

/**
 * Stripe Driver
 * @namespace app.drivers.stripe
 * @author Alexandre Pereira <alex@gooood.biz>
 * @param {Object} app - app namespace to update
 *
 * @returns {Object} knex driver
 */
module.exports = (app) => {
    const Config = app.config

    return knex({
        client: 'pg',
        connection: {
            host : Config.postgresql.host,
            user : Config.postgresql.user,
            password : Config.postgresql.password,
            database : Config.postgresql.database
        }
    })
}