const fs = require('fs')
const Knex = require('knex')
const bcrypt = require('bcrypt')

/**
 * Blockbase Stripe Demo Init Script
 * @author Alexandre Pereira <alex@gooood.biz>
 */

const blockbase = require('@blacksmithstudio/blockbase')

blockbase({ root : `${__dirname}/../../src/` }, async (app) => {
    const Config = app.config 
    const Logger = app.drivers.logger

    const Administrator = app.models.administrator

    Logger.warn('Strip Demo | Setup', 'starting... initializing database')

    let knex = Knex({
        client: 'pg',
        connection: {
            host : Config.postgresql.host,
            user : Config.postgresql.user,
            password : Config.postgresql.password,
            database : 'postgres'
        }
    })

    Logger.warn('Strip Demo | Setup', `creating database "${Config.postgresql.database}"`)
    await knex.raw(`CREATE DATABASE ${Config.postgresql.database}`)

    knex = Knex({
        client: 'pg',
        connection: {
            host : Config.postgresql.host,
            user : Config.postgresql.user,
            password : Config.postgresql.password,
            database : Config.postgresql.database
        }
    })
    const schema = fs.readFileSync(`${__dirname}/../postgresql/schema.sql`).toString()
    Logger.warn('Strip Demo | Setup', `loading SQL schema in "${Config.postgresql.database}"`)
    await knex.raw(schema)

    let admin = new Administrator({ 
        email : 'admin@tillersystems.com',
        password : bcrypt.hashSync('admin1234', 10)
    })
    await admin.save()

    Logger.success('Strip Demo | Setup', 'finished 💪 💪 !!')    
})
