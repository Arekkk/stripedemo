-- DB PSOL SCHEMA, 
-- please create before the "stripedemo" db. 

-- payments
CREATE TABLE "payments" (
    "id" VARCHAR(128) NOT NULL,
    "type" VARCHAR(512) NOT NULL,
    "token" VARCHAR(512) NOT NULL,
    "charge" VARCHAR(512) NOT NULL,
    "customer" VARCHAR(128) NOT NULL,
    "amount" FLOAT NOT NULL,
    "created_at" timestamp with time zone,
    "updated_at" timestamp with time zone
);

CREATE INDEX ON "payments" ("id");
CREATE INDEX ON "payments" ("token");
CREATE INDEX ON "payments" ("charge");
CREATE INDEX ON "payments" ("customer");

-- customers
CREATE TABLE "customers" (
    "id" VARCHAR(128) NOT NULL,
    "email" VARCHAR(512) NOT NULL,
    "firstname" VARCHAR(128) DEFAULT NULL,
    "lastname" VARCHAR(128) DEFAULT NULL,
    "logged_at" timestamp with time zone,
    "created_at" timestamp with time zone,
    "updated_at" timestamp with time zone
);

CREATE INDEX ON "customers" ("id");
CREATE INDEX ON "customers" ("email");

-- administrators
CREATE TABLE "administrators" (
    "id" VARCHAR(128) NOT NULL,
    "email" VARCHAR(512) NOT NULL,
    "password" VARCHAR(1024) NOT NULL,
    "logged_at" timestamp with time zone,
    "created_at" timestamp with time zone,
    "updated_at" timestamp with time zone
);

CREATE INDEX ON "administrators" ("id");
CREATE INDEX ON "administrators" ("email");
