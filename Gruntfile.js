module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
        browserify: {
            files: ['src/views/src/**/*.js'],
            tasks: ['browserify']
        },
        css: {
            files: ['src/views/assets/**/*.scss'],
            tasks: ['compass']
        } 
    },
  
    browserify: {
        dist: {
            options: {
                transform: [['babelify', { presets: ['es2015', 'react'] }]]
            },        
            src: ['src/views/src/app.js'],
            dest: 'src/views/assets/build/app.build.js',
        }
    },

    uglify: {
        build: {
            files: [
                {
                    expand: false,
                    src: ['src/views/assets/build/app.build.js'],
                    dest: 'src/views/assets/build/app.build.min.js'
                }
            ]
        }
    },
    
    compass: {
      dist: {
        options: {
          config: 'config.rb',  // css_dir = 'dev/css'
          cssDir: 'src/views/assets/css'
        }
      }
    },

    // cssmin : {
    // 	target : {
    // 		src : ["src/views/stylesheets/*.css"],
    // 		dest : "src/views/assets/build/blockbase.build.min.css"
    // 	}
    // },

    // watch: {
    //   js: {
    //       files: ['src/views/src/**/*.js'],
    //       tasks: ['browserify', 'uglify']
    //   },
    //   css: {
    //     files: ['src/views/src/**/*.scss'],
    //     tasks: ['compass', 'cssmin']
    //   }
    // }
  });

  // Load the plugin that provides the "browserify" task.
  grunt.loadNpmTasks('grunt-browserify');

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Load the plugin that provides the "compass" task.
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Load the plugin that provides the "compass" task.
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Load the plugin that provides the "contrib-watch" task.
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['browserify', 'compass']);
};
